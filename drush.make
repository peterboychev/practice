; This file will download the projects used by this site
; Run it as ...
; drush make --no-core --contrib-destination=. drush.make .

core = 7.x
api = 2

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[features][version] = "2.2"
projects[features][subdir] = "contrib"

;projects[features_extra][version] = "1.0-beta1"
;projects[features_extra][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[ctools][version] = "1.4"
projects[ctools][subdir] = "contrib"

;projects[wysiwyg][version] = "2.2"
;projects[wysiwyg][subdir] = "contrib"

;libraries[tinymce][download][type] = get
;libraries[tinymce][download][url] = http://download.moxiecode.com/tinymce/tinymce_3.5.11.zip
;libraries[tinymce][directory_name] = tinymce
;libraries[tinymce][destination] = libraries

projects[danland][version] = "1.0"

;projects[jquery_update][version] = "2.4"
;projects[jquery_update][subdir] = "contrib"

;projects[node_export][version] = "3.0"
;projects[node_export][subdir] = "contrib"

;projects[uuid][version] = "1.0-alpha5"
;projects[uuid][subdir] = "contrib"

;projects[date][version] = "2.8"
;projects[date][subdir] = "contrib"

;projects[link][version] = "1.2"
;projects[link][subdir] = "contrib"

;projects[diff][version] = "3.2"
;projects[diff][subdir] = "contrib"

;projects[media][version] = "1.4"
;projects[media][subdir] = "contrib"

;projects[colorbox][version] = "2.7"
;projects[colorbox][subdir] = "contrib"

;libraries[colorbox][download][type] = get
;libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/archive/1.x.zip
;libraries[colorbox][directory_name] = colorbox
;libraries[colorbox][destination] = libraries

;projects[libraries][version] = "2.2"
;projects[libraries][subdir] = "contrib"

;projects[ds][version] = "2.6"
;projects[ds][subdir] = "contrib"

;projects[feeds][version] = "2.0-alpha8"
;projects[feeds][subdir] = "contrib"

;projects[job_scheduler][version] = "2.0-alpha3"
;projects[job_scheduler][subdir] = "contrib"

;projects[feeds_tamper][version] = "1.0"
;projects[feeds_tamper][subdir] = "contrib"

;projects[migrate][version] = "2.5"
;projects[migrate][subdir] = "contrib"
